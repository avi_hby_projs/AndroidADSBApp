package com.example.asingh3.mfrfadsb;

/**
 * @author asingh3
 *
 */
public class AdsbPositionModel {

	private byte srvlncStatus;
	private byte nicSB;
	private byte time;
	private byte cprFrameFlag;
	private byte altitudeQBit;
	private int altitude;
	private int latitudeCPR;
	private int longitudeCPR;
	private long timestamp;

	public AdsbPositionModel()
	{
		this.srvlncStatus = 0;
		this.nicSB = 0;
		this.time = 0;
		this.cprFrameFlag = 0;
		this.altitudeQBit = 0;
		this.altitude = 0;
		this.latitudeCPR = 0;
		this.longitudeCPR = 0;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public byte getAltitudeQBit() {
		return altitudeQBit;
	}

	public void setAltitudeQBit(byte altitudeQBit) {
		this.altitudeQBit = altitudeQBit;
	}

	public byte getSrvlncStatus() {
		return srvlncStatus;
	}

	public void setSrvlncStatus(byte srvlncStatus) {
		this.srvlncStatus = srvlncStatus;
	}

	public byte getNicSB() {
		return nicSB;
	}

	public void setNicSB(byte nicSB) {
		this.nicSB = nicSB;
	}

	public byte getTime() {
		return time;
	}

	public void setTime(byte time) {
		this.time = time;
	}

	public byte getCprFrameFlag() {
		return cprFrameFlag;
	}

	public void setCprFrameFlag(byte cprFrameFlag) {
		this.cprFrameFlag = cprFrameFlag;
	}

	public int getAltitude() {
		return altitude;
	}

	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}

	public int getLatitudeCPR() {
		return latitudeCPR;
	}

	public void setLatitudeCPR(int latitudeCPR) {
		this.latitudeCPR = latitudeCPR;
	}

	public int getLongitudeCPR() {
		return longitudeCPR;
	}

	public void setLongitudeCPR(int longitudeCPR) {
		this.longitudeCPR = longitudeCPR;
	}
}
