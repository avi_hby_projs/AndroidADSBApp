package com.example.asingh3.mfrfadsb;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private static String ACTIVITY_KEY = "DETAILS";

    private TextView icaoTextView;
    private TextView callSignValTextView;
    private TextView airspeedValTextView;
    private TextView airspeedTypeValTextView;
    private TextView groundSpeedValTextView;
    private TextView altitudeValTextView;
    private TextView headingValTextView;
    private TextView vertRateValTextView;
    private TextView vertRateSrcValTextView;
    private TextView locationValTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        icaoTextView = (TextView) findViewById(R.id.icaoTextView);
        callSignValTextView = (TextView) findViewById(R.id.callSignValTextView);
        airspeedValTextView = (TextView) findViewById(R.id.airspeedValTextView);
        airspeedTypeValTextView = (TextView) findViewById(R.id.airspeedTypeValTextView);
        groundSpeedValTextView = (TextView) findViewById(R.id.groundSpeedValTextView);
        altitudeValTextView = (TextView) findViewById(R.id.altitudeValTextView);
        headingValTextView = (TextView) findViewById(R.id.headingValTextView);
        vertRateValTextView = (TextView) findViewById(R.id.vertRateValTextView);
        vertRateSrcValTextView = (TextView) findViewById(R.id.vertRateSrcValTextView);
        locationValTextView = (TextView) findViewById(R.id.locationValTextView);

        // my_child_toolbar is defined in the layout file
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.details_activity_toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        String key = getIntent().getStringExtra(ACTIVITY_KEY);
        AdsbObject selectedItem = DataListActivity.data.getAdsbObject(key);

        String icao = selectedItem.getIcao();
        String callSign = selectedItem.getCallSign();
        float airspeed = selectedItem.getAirSpeed();
        String airspeedStr = "NA";
        if (airspeed != AdsbObject.INVALID_SPEED)
            airspeedStr = String.format("%.2f Knots", airspeed);
        byte airspeedType = selectedItem.getAirSpeedType();
        String airspeedTypeStr = null;
        if (airspeedType == 0)
            airspeedTypeStr = "IAS";
        else if (airspeedType == 1)
            airspeedTypeStr = "TAS";
        float groundSpeed = selectedItem.getGroundSpeed();
        String groundSpeedStr = "NA";
        if (groundSpeed != AdsbObject.INVALID_SPEED)
            groundSpeedStr = String.format("%.2f Knots", groundSpeed);
        int altitude = selectedItem.getAltitude();
        String altitudeStr = "NA";
        if (altitude != AdsbObject.INVALID_ALT)
            altitudeStr = String.format("%d feet", altitude);
        float heading = selectedItem.getHeading();
        String headingStr = "NA";
        if (heading != AdsbObject.INVALID_HEADING)
            headingStr = String.format("%.2f°", heading);
        int vr = selectedItem.getVerticalRate();
        byte vrMovement = selectedItem.getVerticalRateMovement();
        String vrStr = String.format("%d feet/min ", vr);
        String vrDirection = null;
        if(vr == 0)
            vrDirection = " ◄►";
        else if(vrMovement == 0)
            vrDirection = " ▲";
        else if(vrMovement == 1)
            vrDirection = " ▼";
        vrStr = vrStr + vrDirection;
        byte vrSrc = selectedItem.getVerticalRateSource();
        String vrSrcStr = null;
        if(vrSrc == 0)
            vrSrcStr = "Baro-pressure altitude change rate";
        else if(vrSrc == 1)
            vrSrcStr = "Geometric altitude change rate";
        double latitude = selectedItem.getLatitude();
        double longitude = selectedItem.getLongitude();
        String locationStr = "NA";
        if (latitude != AdsbObject.INVALID_COORD && longitude != AdsbObject.INVALID_COORD)
            locationStr = String.format("%.5f, %.5f", latitude, longitude);

        icaoTextView.setText(icao);
        callSignValTextView.setText(callSign);
        airspeedValTextView.setText(airspeedStr);
        airspeedTypeValTextView.setText(airspeedTypeStr);
        groundSpeedValTextView.setText(groundSpeedStr);
        altitudeValTextView.setText(altitudeStr);
        headingValTextView.setText(headingStr);
        vertRateValTextView.setText(vrStr);
        vertRateSrcValTextView.setText(vrSrcStr);
        locationValTextView.setText(locationStr);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    public static Intent newIntent(Context ctx, String key_data)
    {
        Intent i = new Intent(ctx, DetailsActivity.class);
        i.putExtra(ACTIVITY_KEY, key_data);
        return i;
    }


}
