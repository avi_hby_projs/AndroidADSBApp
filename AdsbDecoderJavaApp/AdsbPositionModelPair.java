package com.avi.adsbdec;

public class AdsbPositionModelPair {
    private AdsbPositionModel evenModel;
    private AdsbPositionModel oddModel;

	public AdsbPositionModelPair()
	{
		this.evenModel = null;
		this.oddModel = null;
	}

	public AdsbPositionModel getEvenModel() {
		return evenModel;
	}

	public void setEvenModel(AdsbPositionModel evenModel) {
		this.evenModel = evenModel;
	}

	public AdsbPositionModel getOddModel() {
		return oddModel;
	}

	public void setOddModel(AdsbPositionModel oddModel) {
		this.oddModel = oddModel;
	}

}
