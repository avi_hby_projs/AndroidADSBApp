package com.avi.adsbdec;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author asingh3
 *
 */
public class AdsbDec {

    private static char[] ACID_LOOKUP = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ#####_###############0123456789######".toCharArray();
    private static int NZ = 15;
    private static double D_LAT_EVEN = 360.0 / (4 * NZ);
    private static double D_LAT_ODD = 360.0 / ((4 * NZ) - 1);

    private HashMap<String, AdsbPositionModelPair> modelPairMap;

    public AdsbDec()
    {
        modelPairMap = new HashMap<String, AdsbPositionModelPair>();
    }

    public AdsbObject decode(byte[] data)
    {
        AdsbObject adsbData = new AdsbObject();

        boolean validity = isDataValid(data);

        if(!validity)
            return null;

        String icaoStr = getIcao(data);
        adsbData.setIcao(icaoStr);

        int typeCode = getTypeCode(data);
        adsbData.setTypeCode(typeCode);

        if(typeCode >= 1 && typeCode <= 4)
        {
            String acid = getAcid(data);
            adsbData.setCallSign(acid);
        }
        else if(typeCode >= 9 && typeCode <= 18)
        {
            AdsbPositionModel positionModel = getPositionModel(data);
            AdsbPositionModelPair modelPairTemp = null;

            // Altitude calculations
            int n = positionModel.getAltitude();
            byte qBit = positionModel.getAltitudeQBit();
            int mulFactor = 0;

            if(qBit == 0)
                mulFactor = n * 100;
            else if(qBit == 1)
                mulFactor = n * 25;

            int finalAlt = mulFactor - 1000;
            adsbData.setAltitude(finalAlt);

            // Get even odd message pair
            byte modelType = positionModel.getCprFrameFlag();
            adsbData.setCprFrameFlag(modelType);

            if(modelPairMap.containsKey(icaoStr))
                modelPairTemp = modelPairMap.get(icaoStr);
            else
                modelPairTemp = new AdsbPositionModelPair();

            if(modelType == 0)
                modelPairTemp.setEvenModel(positionModel);
            else if(modelType == 1)
                modelPairTemp.setOddModel(positionModel);

            modelPairMap.put(icaoStr, modelPairTemp);

            // Start calculation once we get even odd message pair
            if((modelPairTemp.getEvenModel() != null) && (modelPairTemp.getOddModel() != null))
            {
                AdsbPositionModel even = modelPairTemp.getEvenModel();
                AdsbPositionModel odd = modelPairTemp.getOddModel();
                double cprLatEven = even.getLatitudeCPR() / 131072.0;
                double cprLongEven = even.getLongitudeCPR() / 131072.0;
                double cprLatOdd = odd.getLatitudeCPR() / 131072.0;
                double cprLongOdd = odd.getLongitudeCPR() / 131072.0;

                // Latitude calculations
                double latFinal = 0.0;
                int j = (int) Math.floor((59 * cprLatEven) - (60 * cprLatOdd) + 0.5);
                double latEven = D_LAT_EVEN * ((j % 60) + cprLatEven);
                double latOdd = D_LAT_ODD * ((j % 59) + cprLatOdd);

                if(latEven >= 270)
                    latEven -= 360;

                if(latEven <= -270)
                    latEven += 360;

                if(latOdd >= 270)
                    latOdd -= 360;

                if(latOdd <= -270)
                    latOdd+= 360;

                if(even.getTimestamp() > odd.getTimestamp())
                    latFinal = latEven;
                else
                    latFinal = latOdd;

                adsbData.setLatitude(latFinal);

                // Longitude calculations
                double longFinal = 0.0;
                int nlLatEven = cprNL(latEven);
                int nlLatOdd = cprNL(latOdd);

                // If NL(Lat_E) and NL(Lat_O) are not the same, two positions are located at
                // different latitude zones. Computation of a global longitude is not possible.
                if(nlLatEven != nlLatOdd)
                    return adsbData;

                if(even.getTimestamp() > odd.getTimestamp())
                {
                    int ni = Math.max(nlLatEven, 1);
                    float dLon = (float) (360.0 / ni);
                    int m = (int) Math.floor((cprLongEven * (nlLatEven - 1)) - (cprLongOdd * nlLatEven) + 0.5);
                    longFinal = dLon * (Util.mod(m, ni) + cprLongEven);
                }
                else
                {
                    int ni = Math.max((nlLatOdd - 1), 1);
                    float dLon = (float) (360.0 / ni);
                    int m = (int) Math.floor((cprLongEven * (nlLatOdd - 1)) - (cprLongOdd * nlLatOdd) + 0.5);
                    longFinal = dLon * (Util.mod(m, ni) + cprLongOdd);
                }

                if(longFinal >= 180.0)
                    longFinal -= 360.0;

                adsbData.setLongitude(longFinal);

                modelPairMap.remove(icaoStr);
            }


        }
        else if(typeCode == 19)
        {
            byte[] speedModelArray = Arrays.copyOfRange(data, 4, 11);
            AdsbVelocityModel velocityModel = getVelocityModel(speedModelArray);

            int subType = velocityModel.getSubType();
            int vEW = 0;
            int vNS = 0;

            if(subType == 1)
            {
                int vEWfac = velocityModel.getVelocityEW() - 1;
                int vNSfac = velocityModel.getVelocityNS() - 1;

                if(velocityModel.getVelocitySignEW() == 1)
                    vEW = -1 * vEWfac;
                else
                    vEW = vEWfac;

                if(velocityModel.getVelocitySignNS() == 1)
                    vNS = -1 * vNSfac;
                else
                    vNS = vNSfac;

                float velocity = (float) Math.sqrt((vEW * vEW) + (vNS * vNS));
                float heading = (float) (Math.atan2(vEW, vNS) * (360 / (2 * Math.PI)));
                if(heading < 0)
                    heading += 360;

                adsbData.setGroundSpeed(velocity);
                adsbData.setHeading(heading);

            }
            else if(subType == 3)
            {
                byte airSpeedType = velocityModel.getAirspeedType();
                int airSpeed = velocityModel.getAirspeed();
                int headingProp = velocityModel.getHeading();
                float heading = (float) ((headingProp / 1024.0) * 360.0);

                adsbData.setAirSpeedType(airSpeedType);
                adsbData.setAirSpeed(airSpeed);
                adsbData.setHeading(heading);
            }

            int verticalRate = (velocityModel.getVerticalRate() - 1) * 64;
            adsbData.setVerticalRate(verticalRate);
            adsbData.setVerticalRateMovement(velocityModel.getVerticalRateSign());
            adsbData.setVerticalRateSource(velocityModel.getVerticalRateSource());
        }

        return adsbData;
    }

    private boolean isDataValid(byte[] data)
    {
        // Check for data length of 112 bits
        // Check for downlink format 17
        // Check for CRC

        return true;
    }

    private String getIcao(byte[] data)
    {
        byte[] icao = Arrays.copyOfRange(data, 1, 4);
        String bitStr = Util.byteArrayToHexStr(icao);

        return bitStr;
    }

    private String getAcid(byte[] data)
    {
        byte[] acidArray = Arrays.copyOfRange(data, 5, 11);

        String bitStr = Util.byteArrayToHexStr(acidArray);

        char[] acid = new char[8];
        int acidIndex = 0;

        for(int i = 0; i < 4; ++i)
        {
            int base = i * 3;
            byte nibble0 = (byte) Character.digit(bitStr.charAt(base), 16);
            byte nibble1 = (byte) Character.digit(bitStr.charAt(base + 1), 16);
            byte nibble2 = (byte) Character.digit(bitStr.charAt(base + 2), 16);
            int fullData = (nibble0 << 8) | (nibble1 << 4) | nibble2;

            int ch0Index = fullData >> 6;
            int ch1Index = fullData & 63;

            acid[acidIndex] = ACID_LOOKUP[ch0Index];
            ++acidIndex;
            acid[acidIndex] = ACID_LOOKUP[ch1Index];
            ++acidIndex;
        }

        String acidStr = new String(acid);

        return acidStr;
    }

    private int getTypeCode(byte[] data)
    {
        int typeCode = (data[4] >> 3) & 0x1F;

        return typeCode;
    }

    private AdsbVelocityModel getVelocityModel(byte[] bArray)
    {
        AdsbVelocityModel model = new AdsbVelocityModel();

        // Speed sub type
        int speedSubType = bArray[0] & 7;
        model.setSubType(speedSubType);

        if(speedSubType == 1)
        {
            // East-West velocity sign
            byte vSignEW = (byte) ((bArray[1] & 0x04) >> 2);
            model.setVelocitySignEW(vSignEW);

            // East-West velocity
            int data0 = bArray[1] & 0x03;
            int data1 =  bArray[2] & 0xFF;
            int vEW = ((data0 << 8) | data1) & 0x03FF;
            model.setVelocityEW(vEW);

            // North-South velocity sign
            byte vSignNS = (byte) ((bArray[3] & 0x80) >> 7);
            model.setVelocitySignNS(vSignNS);

            // North-South velocity
            data0 = bArray[3] & 0x7F;
            data1 = (bArray[4] >> 5) & 0x07;
            int vNS = ((data0 << 3) | data1) & 0x03FF;
            model.setVelocityNS(vNS);
        }
        else if(speedSubType == 3)
        {
            // Heading status
            byte headingStatus = (byte) ((bArray[1] & 0x04) >> 2);
            model.setHeadingStatus(headingStatus);

            // Heading
            int data0 = bArray[1] & 0x03;
            int data1 =  bArray[2] & 0xFF;
            int heading = ((data0 << 8) | data1) & 0x03FF;
            model.setHeading(heading);

            // Airspeed Type
            byte asType = (byte) ((bArray[3] & 0x80) >> 7);
            model.setAirspeedType(asType);

            // Airspeed
            data0 = bArray[3] & 0x7F;
            data1 =  (bArray[4] >> 5) & 0x07;
            int as = ((data0 << 3) | data1) & 0x03FF;
            model.setAirspeed(as);
        }

        // Vertical rate source
        byte vrSrc = (byte) ((bArray[4] >> 4) & 0x01);
        model.setVerticalRateSource(vrSrc);

        // Vertical rate sign
        byte vRSign = (byte) ((bArray[4] >> 3) & 0x01);
        model.setVerticalRateSign(vRSign);

        // Vertical rate
        int data0 = bArray[4] & 0x07;
        int data1 =  (bArray[5] >> 2) & 0x3F;
        int vRate = ((data0 << 6) | data1) & 0x03FF;
        model.setVerticalRate(vRate);

        return model;
    }

    private AdsbPositionModel getPositionModel(byte[] dataByteArry)
    {
        AdsbPositionModel model = new AdsbPositionModel();

        // Surveillance status
        byte ss = (byte) ((dataByteArry[4] >> 1) & 0x03);
        model.setSrvlncStatus(ss);

        // NIC supplement-B
        byte nic = (byte) (dataByteArry[4] & 0x01);
        model.setNicSB(nic);

        // Altitude
        int data0 = (dataByteArry[5] >> 1) & 0x7F;
        int data1 = (dataByteArry[6] >> 4) & 0x0F;
        int altitude = ((data0 << 4) | data1) & 0x07FF;
        model.setAltitude(altitude);

        // Altitude Q bit
        byte qBit = (byte) (data0 & 0x01);
        model.setAltitudeQBit(qBit);

        // Time
        byte time = (byte) ((dataByteArry[6] >> 3) & 0x01);
        model.setTime(time);

        // CPR odd/even frame flag
        byte frameFlag = (byte) ((dataByteArry[6] >> 2) & 0x01);
        model.setCprFrameFlag(frameFlag);

        // Latitude in CPR format
        data0 = dataByteArry[6] & 0x03;
        data1 = dataByteArry[7] & 0xFF;
        int data2 = (dataByteArry[8] >> 1) & 0x7F;
        int latitude = ((data0 << 15) | (data1 << 7) | data2) & 0x1FFFF;
        model.setLatitudeCPR(latitude);

        // Longitude in CPR format
        data0 = dataByteArry[8] & 0x01;
        data1 = dataByteArry[9] & 0xFF;
        data2 = dataByteArry[10] & 0xFF;
        int longitude = ((data0 << 16) | (data1 << 8) | data2) & 0x1FFFF;
        model.setLongitudeCPR(longitude);

        // Timestamp
        long t = System.currentTimeMillis();
        model.setTimestamp(t);

        return model;
    }

    private int cprNL(double cprLat)
    {
        int nl = 0;

        float a = (float) (1 - Math.cos(Math.PI / (2 * NZ)));
        float b = (float) Math.pow(Math.cos(Math.PI / 180.0 * Math.abs(cprLat)), 2);
        float nlF = (float) (2 * Math.PI / (Math.acos(1 - a/b)));
        nl = (int) Math.floor(nlF);

        return nl;
    }

    public static void main(String[] args) {

        String[] adsbStr = {
                "8D4840D6202CC371C32CE0576098",
                "8D485020994409940838175B284F",
                "8DA05F219B06B6AF189400CBC33F",
                "8D40621D58C386435CC412692AD6",
                "8D40621D58C382D690C8AC2863A7",

                "8D75804B580FF2CF7E9BA6F701D0",
                "8D75804B580FF6B283EB7A157117",

                /*"8D75805B9944F033C0045DA67C63",
                "8D75805B58C392AC308A95C63185",
                "8D75805B58C392AC7A8A80C6EFBA",
                "8D75805B9944F033C0045E599471",
                "8D75805B9944EF33C8045E16F883",
                "8D75805B200C50B9DB6820B0ED78",
                "8D75805B9944EF33C8045E16F883",
                "8D75805B58C392AED889D234AC39",
                "8D75805B9944EF33C0045E785A8B"*/

                /*TEST*/
                /*"8D4840D658308641EC770B000000",
                "8D4840D6583082D6907A72000000"*/
                /*"8D4840D65830879D212E56000000",
                "8D4840D658308014F0B264000000"*/
                };

        AdsbDec decoder = new AdsbDec();
        System.out.println("##################################################");

        for (String item : adsbStr) {

            System.out.println("--------------------------------------------------");
            System.out.println("Input ADS-b Packet: " + item);
            System.out.println("------------- Decoded Data -----------------------");

            byte[] adsbItem = Util.hexStrToByteArray(item);

            AdsbObject adsbObj = decoder.decode(adsbItem);

            if(adsbObj == null)
                System.out.println("INVALID DATA !!!");
            else
                System.out.println("<<VALID DATA>>");

            String cprMsgType = "N/A";

            if(adsbObj.getCprFrameFlag() == 0)
                cprMsgType = "Even";
            else if(adsbObj.getCprFrameFlag() == 1)
                cprMsgType = "Odd";

            System.out.println("CPR Message Type: " + cprMsgType);
            System.out.println("ICAO: " + adsbObj.getIcao());
            System.out.println("Type Code: " + adsbObj.getTypeCode());
            System.out.println("ACID: " + adsbObj.getCallSign());
            System.out.println("Ground Speed: " + adsbObj.getGroundSpeed() + " Knots");

            String airSpeedTypeStr = null;
            byte airSpeedType = adsbObj.getAirSpeedType();
            if(airSpeedType == 0)
                airSpeedTypeStr = " (IAS)";
            else if(airSpeedType == 1)
                airSpeedTypeStr = " (TAS)";

            System.out.println("Airspeed: " + adsbObj.getAirSpeed() + " Knots" + airSpeedTypeStr);
            System.out.println("Heading: " + adsbObj.getHeading() + "\u00b0");

            String vrDirection = null;
            String vrSrc = null;
            if(adsbObj.getVerticalRate() == 0)
                vrDirection = " ---";
            else if(adsbObj.getVerticalRateMovement() == 0)
                vrDirection = " Up";
            else if(adsbObj.getVerticalRateMovement() == 1)
                vrDirection = " Down";
            if(adsbObj.getVerticalRateSource() == 0)
                vrSrc = " (Baro-pressure altitude change rate)";
            else if(adsbObj.getVerticalRateSource() == 1)
                vrSrc = " (Geometric altitude change rate)";

            System.out.println("Vertical Rate: " + adsbObj.getVerticalRate() + " ft/min" + vrDirection + vrSrc);

            System.out.println("Latitude: " + String.format("%.5f", adsbObj.getLatitude()));
            System.out.println("Longitude: " + String.format("%.5f", adsbObj.getLongitude()));
            System.out.println("Altitude: " + adsbObj.getAltitude());

        }

        System.out.println("##################################################");


    }

}
