package com.avi.adsbdec;

/**
 * Created by asingh3 on 7/14/2017.
 */

public class AdsbObject {

	public static double INVALID_COORD = 360.0;
	public static float INVALID_HEADING = 500.0F;
	public static float INVALID_SPEED = -1.0F;
	public static int INVALID_ALT = -1;

	private String icao;
    private String callSign;
    private double latitude;
    private double longitude;
    private float groundSpeed;
    private byte airSpeedType;
    private float airSpeed;
    private int altitude;
    private float heading;
    private int verticalRate;
    private byte verticalRateMovement;
    private byte verticalRateSource;
    private long timestamp;
    private int typeCode;
    private byte cprFrameFlag;

    public AdsbObject() {
		this.icao = "NULL";
		this.callSign = "NULL";
		this.latitude = INVALID_COORD;
		this.longitude = INVALID_COORD;
		this.groundSpeed = INVALID_SPEED;
		this.airSpeedType = 0;
		this.airSpeed = INVALID_SPEED;
		this.altitude = INVALID_ALT;
		this.heading = INVALID_HEADING;
		this.verticalRate = 0;
		this.verticalRateMovement = 0;
		this.verticalRateSource = 0;
		this.timestamp = 0;
		this.typeCode = 0;
		this.cprFrameFlag = 9;
	}

	public byte getAirSpeedType() {
		return airSpeedType;
	}

	public byte getCprFrameFlag() {
		return cprFrameFlag;
	}

	public void setCprFrameFlag(byte cprFrameFlag) {
		this.cprFrameFlag = cprFrameFlag;
	}

	public void setAirSpeedType(byte airSpeedType) {
		this.airSpeedType = airSpeedType;
	}

	public int getVerticalRate() {
		return verticalRate;
	}

	public void setVerticalRate(int verticalRate) {
		this.verticalRate = verticalRate;
	}

	public byte getVerticalRateMovement() {
		return verticalRateMovement;
	}

	public void setVerticalRateMovement(byte verticalRateMovement) {
		this.verticalRateMovement = verticalRateMovement;
	}

	public byte getVerticalRateSource() {
		return verticalRateSource;
	}

	public void setVerticalRateSource(byte verticalRateSource) {
		this.verticalRateSource = verticalRateSource;
	}

	public float getGroundSpeed() {
		return groundSpeed;
	}

	public void setGroundSpeed(float groundSpeed) {
		this.groundSpeed = groundSpeed;
	}

	public float getAirSpeed() {
		return airSpeed;
	}

	public void setAirSpeed(float airApeed) {
		this.airSpeed = airApeed;
	}

	public int getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}

	public String getIcao() {
		return icao;
	}

	public void setIcao(String icao) {
		this.icao = icao;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

    public String getCallSign() {
        return callSign;
    }

    public void setCallSign(String callSign) {
        this.callSign = callSign;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public float getHeading() {
        return heading;
    }

    public void setHeading(float heading) {
        this.heading = heading;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMarkerSnippet()
    {
        String snippet = null;

        String position = String.format("Position: %f, %f", latitude, longitude);
        //String speed = String.format("Speed: %f", getSpeed());
        String altitude = String.format("Altitude: %d", getAltitude());
        String heading = String.format("Heading: %d", getHeading());
        snippet = position + "\n" /*+ speed*/ + "\n" + altitude + "\n" + heading;

        return snippet;
    }
}
