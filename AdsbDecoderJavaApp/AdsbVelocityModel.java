package com.avi.adsbdec;

/**
 * @author asingh3
 *
 */
public class AdsbVelocityModel {

	private boolean intentChangeFlag;
	private boolean resvA;
	private byte verticalRateSource;
	private byte velocitySignEW;
	private byte velocitySignNS;
	private byte verticalRateSign;
	private byte baroAltSignDiff;
	private byte headingStatus;
	private byte airspeedType;
	private int airspeed;
	private int heading;
	private int subType;
	private int velocityUncertainty;
	private int velocityEW;
	private int velocityNS;
	private int verticalRate;
	private int resvB;
	private int baroAltDiff;
	
	public AdsbVelocityModel()
	{
		this.intentChangeFlag = false;
		this.resvA = false;
		this.velocitySignEW = 0;
		this.velocitySignNS = 0;
		this.verticalRateSource = 0;
		this.verticalRateSign = 0;
		this.baroAltSignDiff = 0;
		this.headingStatus = 0;
		this.airspeedType = 0;
		this.airspeed = 0;
		this.heading = 0;
		this.subType = 0;
		this.velocityUncertainty = 0;
		this.velocityEW = 0;
		this.velocityNS = 0;
		this.verticalRate = 0;
		this.resvB = 0;
		this.baroAltDiff = 0;
	}

	public boolean isIntentChangeFlag() {
		return intentChangeFlag;
	}

	public void setIntentChangeFlag(boolean intentChangeFlag) {
		this.intentChangeFlag = intentChangeFlag;
	}

	public boolean isResvA() {
		return resvA;
	}

	public void setResvA(boolean resvA) {
		this.resvA = resvA;
	}

	public byte getVerticalRateSource() {
		return verticalRateSource;
	}

	public void setVerticalRateSource(byte verticalRateSource) {
		this.verticalRateSource = verticalRateSource;
	}

	public byte getVelocitySignEW() {
		return velocitySignEW;
	}

	public void setVelocitySignEW(byte velocitySignEW) {
		this.velocitySignEW = velocitySignEW;
	}

	public byte getVelocitySignNS() {
		return velocitySignNS;
	}

	public void setVelocitySignNS(byte velocitySignNS) {
		this.velocitySignNS = velocitySignNS;
	}

	public byte getVerticalRateSign() {
		return verticalRateSign;
	}

	public void setVerticalRateSign(byte verticalRateSign) {
		this.verticalRateSign = verticalRateSign;
	}

	public byte getBaroAltSignDiff() {
		return baroAltSignDiff;
	}

	public void setBaroAltSignDiff(byte baroAltSignDiff) {
		this.baroAltSignDiff = baroAltSignDiff;
	}

	public byte getHeadingStatus() {
		return headingStatus;
	}

	public void setHeadingStatus(byte headingStatus) {
		this.headingStatus = headingStatus;
	}

	public byte getAirspeedType() {
		return airspeedType;
	}

	public void setAirspeedType(byte airspeedType) {
		this.airspeedType = airspeedType;
	}

	public int getAirspeed() {
		return airspeed;
	}

	public void setAirspeed(int airspeed) {
		this.airspeed = airspeed;
	}

	public int getHeading() {
		return heading;
	}

	public void setHeading(int heading) {
		this.heading = heading;
	}

	public int getSubType() {
		return subType;
	}

	public void setSubType(int subType) {
		this.subType = subType;
	}

	public int getVelocityUncertainty() {
		return velocityUncertainty;
	}

	public void setVelocityUncertainty(int velocityUncertainty) {
		this.velocityUncertainty = velocityUncertainty;
	}

	public int getVelocityEW() {
		return velocityEW;
	}

	public void setVelocityEW(int velocityEW) {
		this.velocityEW = velocityEW;
	}

	public int getVelocityNS() {
		return velocityNS;
	}

	public void setVelocityNS(int velocityNS) {
		this.velocityNS = velocityNS;
	}

	public int getVerticalRate() {
		return verticalRate;
	}

	public void setVerticalRate(int verticalRate) {
		this.verticalRate = verticalRate;
	}

	public int getResvB() {
		return resvB;
	}

	public void setResvB(int resvB) {
		this.resvB = resvB;
	}

	public int getBaroAltDiff() {
		return baroAltDiff;
	}

	public void setBaroAltDiff(int baroAltDiff) {
		this.baroAltDiff = baroAltDiff;
	}


}
