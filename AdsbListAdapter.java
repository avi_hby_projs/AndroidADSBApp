package com.example.asingh3.mfrfadsb;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by asingh3 on 8/28/2017.
 */

class AdsbListAdapter extends ArrayAdapter<AdsbObject> {

    private Context context;
    private ArrayList<AdsbObject> myData;

    static class ViewHolder
    {
        public TextView icaoTextView;
        public TextView callSignTextView;
        public TextView airspeedValTextView;
        public TextView altValTextView;
        public TextView headingValTextView;
    }

    public AdsbListAdapter(@NonNull Context context, @NonNull ArrayList<AdsbObject> objects) {

        super(context, R.layout.row_layout, objects);
        this.context = context;
        this.myData = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.icaoTextView = (TextView) convertView.findViewById(R.id.icaoTextView);
            viewHolder.callSignTextView = (TextView) convertView.findViewById(R.id.callSignTextView);
            viewHolder.airspeedValTextView = (TextView) convertView.findViewById(R.id.airspeedValTextView);
            viewHolder.altValTextView = (TextView) convertView.findViewById(R.id.altValTextView);
            viewHolder.headingValTextView = (TextView) convertView.findViewById(R.id.headingValTextView);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();

        String icao = myData.get(position).getIcao();
        String callSign = myData.get(position).getCallSign();
        int altVal = myData.get(position).getAltitude();
        float speedVal = myData.get(position).getAirSpeed();
        float headingVal = myData.get(position).getHeading();
        String altValStr = "NA";
        if (altVal != AdsbObject.INVALID_ALT)
            altValStr = String.format("%d feet", altVal);
        String speedValStr = "NA";
        if (speedVal != AdsbObject.INVALID_SPEED)
            speedValStr = String.format("%.2f Knots", speedVal);
        String headingValStr = "NA";
        if (headingVal != AdsbObject.INVALID_HEADING)
            headingValStr = String.format("%.2f°", headingVal);

        holder.icaoTextView.setText(icao);
        holder.callSignTextView.setText(callSign);
        holder.airspeedValTextView.setText(speedValStr);
        holder.altValTextView.setText(altValStr);
        holder.headingValTextView.setText(headingValStr);

        return convertView;
    }

}
