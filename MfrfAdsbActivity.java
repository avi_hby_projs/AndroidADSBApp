package com.example.asingh3.mfrfadsb;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.concurrent.atomic.AtomicBoolean;

public class MfrfAdsbActivity extends AppCompatActivity implements OnMapReadyCallback{

    public static final int CLEAR_MAP = 0;
    public static final int UPDATE_MAP = 1;
    public static final int UPDATE_MAP_BB = 2;

    private AtomicBoolean isConnected;
    private AtomicBoolean isMarkerSelected;

    private Button showDetailsBtn;
    private GoogleMap mMap;
    private AdsbData adsbData;
    private AdsbManager adsbManager;
    private MapRefreshSweep mapRefreshSweep;
    private Handler handle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mfrf_adsb);

        isConnected = new AtomicBoolean(true);
        isMarkerSelected = new AtomicBoolean(false);

        Toolbar mTb = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mTb);

        showDetailsBtn = (Button) findViewById(R.id.showDetailsBtn);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);

        adsbData = new AdsbData();
        DataListActivity.data = adsbData;

        showDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = DataListActivity.newIntent(MfrfAdsbActivity.this);
                startActivity(i);
            }
        });

        handle = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message inMsg)
            {
                switch (inMsg.what)
                {
                    case CLEAR_MAP :    clearMap();
                                        break;
                    case UPDATE_MAP:    AdsbObject adsbObject = (AdsbObject) inMsg.obj;
                                        updateMap(adsbObject);
                                        break;
                    case UPDATE_MAP_BB: MapBoundingBox mapBoundingBox = (MapBoundingBox) inMsg.obj;
                                        updateMapBoundingBox(mapBoundingBox);
                                        break;
                    default        :    break;
                }
            }
        };

        mapRefreshSweep = new MapRefreshSweep(isConnected, isMarkerSelected, adsbData, handle);
        adsbManager = new AdsbManager(isConnected, adsbData);
        Thread adsbManagerTh = new Thread(adsbManager);
        Thread mapRefreshSweepTh = new Thread(mapRefreshSweep);
        adsbManagerTh.start();
        mapRefreshSweepTh.start();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                DialogFragment df = new SettingsDialog();
                df.show(getFragmentManager(), "Settings");
                // User chose the "Settings" item, show the app settings UI...
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LayoutInflater inflater = getLayoutInflater();
        mMap.setInfoWindowAdapter(new CustomMarkerInfoWindowAdapter(inflater));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                isMarkerSelected.set(true);
                String snippet = marker.getSnippet();

                if (isMoving(snippet))
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ac_moving_selected));

                return false;
            }
        });

        mMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
            @Override
            public void onInfoWindowClose(Marker marker) {

                isMarkerSelected.set(false);
                String snippet = marker.getSnippet();

                if (isMoving(snippet))
                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ac_moving));
            }
        });
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        isConnected.set(false);
    }

    private void updateMap(AdsbObject adsbObject)
    {
        if (mMap != null)
        {
            String title = adsbObject.getCallSign();
            String snippet = adsbObject.getMarkerSnippet();
            LatLng location = adsbObject.getLocation();
            float heading = adsbObject.getHeading();
            if (heading == adsbObject.INVALID_HEADING)
                heading = 45.0F;

            int acIconType = 0;
            if (isMoving(snippet))
                acIconType = R.mipmap.ac_moving;
            else
                acIconType = R.mipmap.ac_not_moving;

            mMap.addMarker(new MarkerOptions()
                    .position(location)
                    .title(title)
                    .snippet(snippet)
                    .icon(BitmapDescriptorFactory.fromResource(acIconType))
                    .anchor(0.5F, 0.5F)
                    .rotation(heading));
        }
    }

    private boolean isMoving(String snippet) {

        String[] data = snippet.split(":");
        String airSpeed = data[2];
        String groundSpeed = data[3];

        if (airSpeed.equals("NA") && groundSpeed.equals("NA"))
            return false;
        else
            return true;
    }

    private void clearMap() { mMap.clear(); }

    private void updateMapBoundingBox(MapBoundingBox mapBoundingBox) {

        LatLng downLeft = mapBoundingBox.getDownLeft();
        LatLng topRight = mapBoundingBox.getTopRight();

        if (downLeft.latitude < topRight.latitude)
        {
            LatLngBounds bounds = new LatLngBounds(downLeft, topRight);
            CameraUpdate camera = CameraUpdateFactory.newLatLngBounds(bounds, 200);
            mMap.animateCamera(camera, 2000, null);
        }
    }

    private BitmapDescriptor vectorToBitmap(@DrawableRes int id, @ColorInt int color) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
