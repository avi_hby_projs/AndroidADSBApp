package com.example.asingh3.mfrfadsb;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by asingh3 on 7/18/2017.
 */

public class AdsbData {

    private static final int DATA_READ = 0;
    private static final int DATA_WRITE = 1;

    private HashMap<String, AdsbObject> data;

    public AdsbData()
    {
        data = new HashMap<String, AdsbObject>();
    }

    public synchronized void addAdsbObject(AdsbObject adsbObject)
    {
        String key = adsbObject.getIcao();

        // Merge data if ICAO is already present
        if (data.containsKey(key))
        {
            AdsbObject temp = data.get(key);
            mergeAdsbObjects(temp, adsbObject);
            data.put(key, temp);
        }
        else
            data.put(key, adsbObject);
    }

    public String[] getActiveItems() {

        String[] activeItems = null;

        Set<String> adsbKeys = data.keySet();
        activeItems = adsbKeys.toArray(new String[data.size()]);

        return activeItems;
    }

    public AdsbObject getAdsbObject(String key)
    {
        return data.get(key);
    }

    private void mergeAdsbObjects(AdsbObject temp, AdsbObject adsbObject)
    {
        String callSign = adsbObject.getCallSign();
        double latitude = adsbObject.getLatitude();
        double longitude = adsbObject.getLongitude();
        float groundSpeed = adsbObject.getGroundSpeed();
        byte airSpeedType = adsbObject.getAirSpeedType();
        float airSpeed = adsbObject.getAirSpeed();
        int altitude = adsbObject.getAltitude();
        float heading = adsbObject.getHeading();
        int verticalRate = adsbObject.getVerticalRate();
        byte verticalRateMovement = adsbObject.getVerticalRateMovement();
        byte verticalRateSource = adsbObject.getVerticalRateSource();
        long timestamp = adsbObject.getTimestamp();
        int typeCode = adsbObject.getTypeCode();
        byte cprFrameFlag = adsbObject.getCprFrameFlag();

        if(typeCode >= 1 && typeCode <= 4)
        {
            temp.setCallSign(callSign);
        }
        else if(typeCode >= 9 && typeCode <= 18)
        {
            temp.setCprFrameFlag(cprFrameFlag);
            temp.setLatitude(latitude);
            temp.setLongitude(longitude);
            temp.setAltitude(altitude);
        }
        else if(typeCode == 19)
        {
            temp.setGroundSpeed(groundSpeed);
            temp.setHeading(heading);
            temp.setAirSpeedType(airSpeedType);
            temp.setAirSpeed(airSpeed);
            temp.setVerticalRate(verticalRate);
            temp.setVerticalRateMovement(verticalRateMovement);
            temp.setVerticalRateSource(verticalRateSource);
        }

        temp.setTimestamp(timestamp);
    }
}
