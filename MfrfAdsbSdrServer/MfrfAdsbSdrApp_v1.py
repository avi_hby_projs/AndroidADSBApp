import socket
import time
import AddressData as data

FILE_NAME = 'ADS-b_data.txt'

cli_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sdr_app_address = (data.IP_SDR_APP, data.PORT_SDR_APP)
android_app_address = (data.IP_AND_APP, data.PORT_AND_APP)
print('Starting ADS-b server on {}, port: {} ...'.format(*sdr_app_address))

cli_sock.bind(sdr_app_address)

data_file = open(FILE_NAME, 'r')

#while True:
    #msg = input("[<--]: ")
    #msg_bytes = bytearray(msg, 'utf-8')
    #cli_sock.sendto(msg_bytes, android_app_address)
    #if msg == "XXX":
    #    break

for line in data_file:
    line = line.rstrip()
    print(line)
    line_bytes = bytearray.fromhex(line)
    cli_sock.sendto(line_bytes, android_app_address)
    time.sleep(0.5)

#cli_sock.sendto(b'XXX', android_app_address)

print("Exiting ADS App Client !!!")
data_file.close()
cli_sock.close()
