import socket
import AddressData as data

serv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sdr_app_address = (data.IP, data.SDR_APP_PORT)
android_app_address = (data.IP, data.AND_APP_PORT)

print('Starting ADS-b Android App server on {}, port: {} ...'.format(*android_app_address))

serv_sock.bind(android_app_address)

while True:
    msg, addr = serv_sock.recvfrom(1024)
    msg_str = str(msg, 'utf-8')
    print("[{0}:{1}-->]: {2}".format(addr[0], addr[1], msg_str))
    if msg_str == "XXX":
        break

print("Exiting Android App Server !!!")
serv_sock.close()
