package com.example.asingh3.mfrfadsb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by asingh3 on 8/29/2017.
 */

public class CustomMarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private LayoutInflater inflater;

    public CustomMarkerInfoWindowAdapter(LayoutInflater inflater)
    {
        this.inflater = inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        View view = inflater.inflate(R.layout.info_window_layout, null);

        TextView icaoTextView = (TextView) view.findViewById(R.id.icaoTextView);
        TextView callSignTextView = (TextView) view.findViewById(R.id.callSignTextView);
        TextView altitudeTextView = (TextView) view.findViewById(R.id.altitudeTextView);
        TextView airspeedTextView = (TextView) view.findViewById(R.id.airspeedTextView);
        TextView headingTextView = (TextView) view.findViewById(R.id.headingTextView);
        TextView locationTextView = (TextView) view.findViewById(R.id.locationTextView);

        String snippet = marker.getSnippet();
        String[] data = snippet.split(":");
        icaoTextView.setText(data[0]);
        callSignTextView.setText(data[1]);
        airspeedTextView.setText(data[2]);
        altitudeTextView.setText(data[4]);
        headingTextView.setText(data[5]);
        locationTextView.setText(data[6]);

        return view;
    }
}
