package com.example.asingh3.mfrfadsb;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by asingh3 on 7/27/2017.
 */

public class MapBoundingBox {

    private LatLng downLeft;
    private LatLng topRight;

    public MapBoundingBox() {

        LatLng downLeft = new LatLng(90.0, 179.0);
        LatLng topRight = new LatLng(-90.0, -180.0);

        this.downLeft = downLeft;
        this.topRight = topRight;
    }

    public MapBoundingBox(LatLng downLeft, LatLng topRight) {
        this.downLeft = downLeft;
        this.topRight = topRight;
    }

    public LatLng getDownLeft() {
        return downLeft;
    }

    public LatLng getTopRight() {
        return topRight;
    }

    public void updateBoundingBox(LatLng pos) {

        double minLat = 0.0;
        double minLong = 0.0;
        double maxLat = 0.0;
        double maxLong = 0.0;

        if (pos.latitude < downLeft.latitude)
            minLat = pos.latitude;
        else
            minLat = downLeft.latitude;

        if (pos.longitude < downLeft.longitude)
            minLong = pos.longitude;
        else
            minLong = downLeft.longitude;

        if (pos.latitude > topRight.latitude)
            maxLat = pos.latitude;
        else
            maxLat = topRight.latitude;

        if (pos.longitude > topRight.longitude)
            maxLong = pos.longitude;
        else
            maxLong = topRight.longitude;

        this.downLeft = new LatLng(minLat, minLong);
        this.topRight = new LatLng(maxLat, maxLong);
    }
}
