package com.example.asingh3.mfrfadsb;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class DataListActivity extends AppCompatActivity {

    public static AdsbData data;

    private ListView adsbDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_list);

        // my_child_toolbar is defined in the layout file
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.data_list_activity_toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        adsbDataList = (ListView) findViewById(R.id.adsbDataList);
        Context ctx = getApplicationContext();

        String[] keys = data.getActiveItems();

        final ArrayList<AdsbObject> adsbObjectList = new ArrayList<AdsbObject>();

        for (String key :
                keys) {
            AdsbObject temp = data.getAdsbObject(key);
            adsbObjectList.add(temp);
        }

        AdsbListAdapter adapter = new AdsbListAdapter(ctx, adsbObjectList);
        adsbDataList.setAdapter(adapter);

        adsbDataList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AdsbObject selectedAdsbObj = adsbObjectList.get(position);
                String key = selectedAdsbObj.getIcao();

                Intent i = DetailsActivity.newIntent(DataListActivity.this, key);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    public static Intent newIntent(Context ctx)
    {
        Intent i = new Intent(ctx, DataListActivity.class);
        return i;
    }
}
