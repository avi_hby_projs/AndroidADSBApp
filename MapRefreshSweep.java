package com.example.asingh3.mfrfadsb;

import android.os.Handler;
import android.os.Message;

import com.google.android.gms.maps.model.LatLng;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by asingh3 on 7/19/2017.
 */

public class MapRefreshSweep implements Runnable {

    private AtomicBoolean isConnected;
    private AtomicBoolean isMarkerSelected;
    private AdsbData adsbData;
    private Handler uiThread;

    public MapRefreshSweep(AtomicBoolean isConnected, AtomicBoolean isMarkerSelected, AdsbData adsbData, Handler uiThread) {
        this.isConnected = isConnected;
        this.isMarkerSelected = isMarkerSelected;
        this.adsbData = adsbData;
        this.uiThread = uiThread;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (isConnected.get())
        {
            String[] dataKeys = adsbData.getActiveItems();
            AdsbObject newData = null;
            MapBoundingBox boundingBox = new MapBoundingBox();

            if (isMarkerSelected.get() == false)
            {
                clearMap();

                for (String item :
                        dataKeys) {
                    newData = adsbData.getAdsbObject(item);

                    LatLng pos = newData.getLocation();

                    if (pos != null)
                    {
                        boundingBox.updateBoundingBox(pos);
                        updateMap(newData);
                    }
                }

                if (dataKeys.length > 0)
                    setMapBoundingBox(boundingBox);
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void setMapBoundingBox(MapBoundingBox boundingBox) {
        Message msg = uiThread.obtainMessage(MfrfAdsbActivity.UPDATE_MAP_BB, boundingBox);
        uiThread.sendMessage(msg);
    }

    private void updateMap(AdsbObject data)
    {
        Message msg = null;

        if (data != null)
            msg = uiThread.obtainMessage(MfrfAdsbActivity.UPDATE_MAP, data);
        else
            return;

        uiThread.sendMessage(msg);
    }

    private void clearMap()
    {
        Message msg = uiThread.obtainMessage(MfrfAdsbActivity.CLEAR_MAP);
        uiThread.sendMessage(msg);
    }
}
