package com.example.asingh3.mfrfadsb;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by asingh3 on 7/18/2017.
 */

public class AdsbManager implements Runnable {

    private static String TAG = "AdsbManager";
    private static int UDP_PORT_APP = 7689;
    private static int UDP_PORT_RADIO = 7685;
    private AtomicBoolean isConnected;
    private AdsbData adsbData;

    public AdsbManager(AtomicBoolean isConnected, AdsbData adsbData) {
        this.isConnected = isConnected;
        this.adsbData = adsbData;
    }

    @Override
    public void run() {

        byte[] dataRecv = new byte[32];
        DatagramSocket clientSocket = null;
        DatagramPacket recvPacket = null;
        AdsbDecoder decoder = new AdsbDecoder();

        try {
            clientSocket = new DatagramSocket(UDP_PORT_APP);
        } catch (SocketException e) {
            e.printStackTrace();
            Log.e(TAG, "Error Binding IP Address !");
        }

        while (isConnected.get())
        {
            recvPacket = new DatagramPacket(dataRecv, dataRecv.length);


            try {
                clientSocket.receive(recvPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }

            byte[] adsbItem = Arrays.copyOfRange(dataRecv, 0, 14);

            AdsbObject adsbObj = decoder.decode(adsbItem);

            if(adsbObj != null)
                adsbData.addAdsbObject(adsbObj);

            String dataStr = Util.byteArrayToHexStr(adsbItem);
            Log.i(TAG, dataStr);

        }

        /*String[] adsbStr = {
                "8D4840D6202CC371C32CE0576098",
                "8D485020994409940838175B284F",
                "8DA05F219B06B6AF189400CBC33F",

                "8D75805B9944F033C0045DA67C63",
                "8D75805B58C392AC308A95C63185",
                "8D75805B58C392AC7A8A80C6EFBA",
                "8D75805B9944F033C0045E599471",
                "8D75805B9944EF33C8045E16F883",
                "8D75805B200C50B9DB6820B0ED78",
                "8D75805B9944EF33C8045E16F883",
                "8D75805B58C392AED889D234AC39",
                "8D75805B9944EF33C0045E785A8B",

                // Engineered
                "8D40621D202CC371C32CE0576098",
                "8D40621D9B06B6AF189400CBC33F",
                "8D40621D5830865666EB7A000000",
                "8D40621D583082A5F677A0000000",

                "8DAC82EC5850A70DB68789000000",
                "8DAC82EC5850A371BE1506000000",

                "8DB0707A58100661834C7B000000",
                "8DB0707A581002C29EDE13000000",

                "8DC3946B5810765DB2217F000000",
                "8DC3946B581072BEBFACDD000000",

                "8DDB82B8587097A99E50B5000000",
                "8DDB82B8587090104BDA99000000",

                "8D75804B58F0F471D08D08000000",
                "8D75804B58F0F0CA8614F8000000"
        };

        AdsbDecoder decoder = new AdsbDecoder();

        for (String item : adsbStr) {

            byte[] adsbItem = Util.hexStrToByteArray(item);

            AdsbObject adsbObj = decoder.decode(adsbItem);

            if(adsbObj != null)
                adsbData.addAdsbObject(adsbObj);
        }*/
    }
}
